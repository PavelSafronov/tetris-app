import { Vector2 } from "ts-vector-math";
import { writeLocalStorage, readLocalStorage, centerText } from "./Utils";
import { Piece, createBoard, getPiece, ArrayPos, forEachCell } from "./Piece";
import { Keyboard, KeyEventType } from "./Keyboard";
import { Props, GamePiece, LocalData, State } from "./Interfaces";
import { drawMultilineText, LineSplitApproach } from "./MultilineCanvasText";
import { Rect } from "./Rect";

export class Game {
  private static readonly localDataKey = "tetris";
  private static readonly downPos = new ArrayPos(-1, 0);
  private static readonly leftPos = new ArrayPos(0, -1);
  private static readonly rightPos = new ArrayPos(0, 1);
  private static readonly watchedKeys: string[] = [
    "KeyD",
    "KeyP",
    // "KeyR",
    "NumpadEnter",
    "Enter",
    "Space",
    "ArrowUp",
    "ArrowLeft",
    "ArrowRight",
    "ArrowDown"
  ];

  private readonly props: Props;
  // @ts-ignore
  // Adding an ignore here because the constructor ALWAYS calls init,
  // and init ALWAYS configures state.
  private state: State;

  // CONSTRUCTOR
  constructor(canvas: HTMLCanvasElement, props: Props) {
    this.props = props;
    this.init(canvas);

    // this.addDrawingListener();

    window.requestAnimationFrame(() => this.renderGame());
  }

  private addDrawingListener() {
    const {
      props: { colors }
    } = this;
    let isDrawing: boolean = false;
    const stopDrawing = () => (isDrawing = false);
    const startDrawing = () => (isDrawing = true);
    window.addEventListener("mousedown", startDrawing);
    window.addEventListener("mouseup", stopDrawing);
    window.addEventListener("mouseleave", stopDrawing);
    window.addEventListener("mouseout", stopDrawing);
    window.addEventListener("mousemove", (ev: MouseEvent) => {
      if (isDrawing) {
        const {
          state: { gridPos, cellSize, currentPiece },
          props: { rows, cols }
        } = this;
        var clickVec = new Vector2([ev.clientX, ev.clientY]);
        clickVec.subtract(gridPos);
        clickVec.scale(1 / cellSize);
        var pos: ArrayPos = new ArrayPos(rows - clickVec.y, clickVec.x);
        const { r, c } = pos;
        if (r >= 0 && r < rows && c >= 0 && c < cols) {
          const cell = this.state.cells[r][c];
          if (cell === 0) {
            const randomColor = Math.floor(
              Math.random() * (colors.length - 1) + 1
            );
            const color = currentPiece ? currentPiece.getColor() : randomColor;
            this.state.cells[r][c] = color;
          }
        }
      }
    });
  }

  // TOP-LEVEL
  private init(canvas: HTMLCanvasElement) {
    const {
      props: { rows, cols, stepPeriodMs }
    } = this;
    const ctx = canvas.getContext("2d");
    if (!ctx) {
      throw new Error();
    }
    const now = new Date().getTime();

    this.state = {
      ctx,
      canvas,
      level: 0,
      score: 0,
      localData: readLocalStorage<LocalData>(Game.localDataKey, () => ({
        isPaused: false,
        showDebug: true
      })),
      keyboard: new Keyboard(Game.watchedKeys, stepPeriodMs / 2),
      startTime: now,
      timeMs: now,
      lastStepMs: 0,
      lastRenderMs: 0,
      cells: createBoard(rows, cols),
      isGameOver: false,
      isPaused: false,
      nextPiece: this.getNextPiece(),
      gridPos: new Vector2(),
      cellSize: 0
    };
  }
  private renderGame(): void {
    const timeMs = new Date().getTime();
    this.state.timeMs = timeMs;
    this.state.lastRenderMs = timeMs;

    const {
      state: { keyboard }
    } = this;

    keyboard.forEachActiveKey((key, keyEvent, nowMs) => {
      const { type } = keyEvent;
      switch (type) {
        case KeyEventType.Down:
          keyEvent.isHandled = this.handleKeyCode(key, true);
          break;
        case KeyEventType.Up:
          this.handleKeyCode(key, false);
          break;
      }
    });

    this.step();
    this.prepareToDraw();
    this.draw();

    window.requestAnimationFrame(() => this.renderGame());
  }
  private step(): void {
    const {
      props: { stepPeriodMs },
      state: {
        cells,
        currentPiece,
        nextPiece,
        isGameOver,
        isPaused,
        timeMs,
        lastStepMs
      }
    } = this;

    if (isPaused || isGameOver) {
      return;
    }
    const stepTimeMs = lastStepMs + stepPeriodMs;
    if (timeMs < stepTimeMs) {
      return;
    }

    this.state.lastStepMs = timeMs;

    if (!currentPiece) {
      if (!this.addNewPiece(nextPiece)) {
        this.state.isGameOver = true;
      }
      this.state.nextPiece = this.getNextPiece();
    }

    if (currentPiece) {
      const { pos } = currentPiece;

      // DEBUG
      if (currentPiece.isOverlap(cells, pos)) {
        console.error(`overlap at pos = ${pos}`);
      }

      const moved = this.moveCurrentPiece(Game.downPos);
      if (!moved) {
        currentPiece.forEachCell((cellPos, cell) => {
          if (cell > 0) {
            cells[cellPos.r + pos.r][cellPos.c + pos.c] = cell;
          }
        });
        this.state.currentPiece = undefined;
      }
    }

    // var removedLines = this.removeCompletedLines();
    // this.updateScore(removedLines);

    var removedLines = this.removeMarkedLines();
    this.updateScore(removedLines);

    this.markCompletedLines();
  }

  // INPUT

  private handleKeyCode(code: string, isStillDown: boolean): boolean {
    const {
      state: { localData, isGameOver, isPaused }
    } = this;
    const blockGameInput = isGameOver || isPaused;
    var isGood = true;

    if (!Game.watchedKeys.includes(code)) {
      console.error(`Unregistered key ${code}`);
    }

    switch (code) {
      case "KeyD":
        if (isStillDown) break;
        localData.showDebug = !localData.showDebug;
        writeLocalStorage<LocalData>(Game.localDataKey, this.state.localData);
        break;
      case "KeyP":
        if (isStillDown) break;
        if (!isGameOver) {
          this.state.isPaused = !this.state.isPaused;
          writeLocalStorage<LocalData>(Game.localDataKey, this.state.localData);
        }
        break;
      // case "KeyR":
      //   if (isStillDown) break;
      //   this.init(canvas);
      //   break;

      case "NumpadEnter":
      case "Enter":
        if (isStillDown) break;
        if (blockGameInput) break;
        while (this.moveCurrentPiece(Game.downPos)) {}
        break;

      case "ArrowUp":
      case "Space":
        if (blockGameInput) break;
        isGood = isGood && this.rotateCurrentPiece();
        break;
      case "ArrowLeft":
        if (blockGameInput) break;
        isGood = isGood && this.moveCurrentPiece(Game.leftPos);
        break;
      case "ArrowRight":
        if (blockGameInput) break;
        isGood = isGood && this.moveCurrentPiece(Game.rightPos);
        break;
      case "ArrowDown":
        if (blockGameInput) break;
        isGood = isGood && this.moveCurrentPiece(Game.downPos);
        break;
    }

    return isGood;
  }

  // DRAWING
  private draw(): void {
    const {
      state: {
        cells,
        isGameOver,
        canvas: { width, height },
        ctx,
        currentPiece,
        score,
        level,
        nextPiece,
        isPaused
      },
      props: { rows, cols }
    } = this;

    const cellWidth = width / cols;
    const cellHeight = height / rows;
    const cellSize = Math.min(cellWidth, cellHeight) - 1;
    const xPadding = (width - cellSize * cols) / 1.1;
    const yPadding = (height - cellSize * rows) / 2;
    const gridPos = new Vector2([xPadding, yPadding]);
    this.state.gridPos = gridPos;
    this.state.cellSize = cellSize;

    this.drawCells(cells, gridPos, cellSize, false);

    if (currentPiece) {
      const pieceRows = currentPiece.cells.length;

      this.drawGhostPiece(
        currentPiece,
        gridPos,
        pieceRows,
        cellSize,
        rows,
        ctx
      );

      this.drawCurrentPiece(gridPos, currentPiece, pieceRows, cellSize, rows);
    }

    ctx.fillStyle = "#4096EE";
    ctx.font = "30px Courier";
    ctx.fillText(`Level: ${level}`, 0, 40);
    ctx.fillText(`Score: ${score}`, 0, 80);
    ctx.fillText(`Next:`, 0, 120);
    const nextPiecePos = new Vector2([100, 100]);
    this.drawCells(nextPiece.cells, nextPiecePos, cellSize, true);

    ctx.fillStyle = "#4096EE";
    drawMultilineText(
      ctx,
      [
        "Controls:",
        "Left/Right -> Move",
        "Space/Up -> Rotate",
        "Enter -> Drop",
        "P -> Pause"
      ],
      {
        rect: new Rect(new Vector2([0, 250]), new Vector2([1000, 1000])),
        lineHeightRatio: 2,
        lineSeparator: "\n",
        splitApproach: LineSplitApproach.SplitByCharacters,
        wordSeparator: ""
      }
    );

    if (isGameOver || isPaused) {
      ctx.fillStyle = "red";
      ctx.strokeStyle = "black";
      ctx.font = "80px Courier";
      var text = isGameOver ? "GAME OVER" : "PAUSED";
      var { dx, dy } = centerText(ctx, text);
      ctx.fillText(text, width / 2 + dx, height / 2 + dy);
      ctx.strokeText(text, width / 2 + dx, height / 2 + dy);
    }
  }

  private drawCurrentPiece(
    gridPos: Vector2,
    currentPiece: GamePiece,
    pieceRows: number,
    cellSize: number,
    rows: number
  ) {
    const piecePos = this.cellposToVec(
      gridPos,
      new ArrayPos(currentPiece.pos.r + pieceRows, currentPiece.pos.c),
      cellSize,
      rows
    );
    this.drawCells(currentPiece.cells, piecePos, cellSize, true);
  }

  private drawGhostPiece(
    currentPiece: GamePiece,
    gridPos: Vector2,
    pieceRows: number,
    cellSize: number,
    rows: number,
    ctx: CanvasRenderingContext2D
  ) {
    const {
      state: { cells }
    } = this;
    var ghostPos = currentPiece.pos.copy();
    while (!currentPiece.isOverlap(cells, ghostPos)) {
      ghostPos.add(Game.downPos);
    }
    if (!currentPiece.pos.equals(ghostPos)) {
      ghostPos.sub(Game.downPos);
    }
    const ghostPosVector = this.cellposToVec(
      gridPos,
      new ArrayPos(ghostPos.r + pieceRows - 1, ghostPos.c),
      cellSize,
      rows
    );

    ctx.save();
    ctx.globalAlpha = 0.99;
    ctx.globalCompositeOperation = "lighter";
    ctx.shadowBlur = 5;
    this.drawCells(currentPiece.cells, ghostPosVector, cellSize, true);
    ctx.restore();
  }

  private drawCells(
    cells: number[][],
    pos: Vector2,
    cellSize: number,
    onlyDrawFilledCells: boolean = true
  ) {
    const {
      state: { ctx },
      props: { colors }
    } = this;

    ctx.strokeStyle = "black";
    const rows = cells.length;
    forEachCell(cells, (cellPos, cell) => {
      if (!onlyDrawFilledCells || (onlyDrawFilledCells && cell > 0)) {
        const vec = this.cellposToVec(pos, cellPos, cellSize, rows);
        ctx.fillStyle = colors[cell];
        ctx.fillRect(vec.x, vec.y, cellSize, cellSize);
        ctx.strokeRect(vec.x, vec.y, cellSize, cellSize);
      }
    });
  }
  private cellposToVec(
    pos: Vector2,
    cellpos: ArrayPos,
    cellSize: number,
    rows: number
  ): Vector2 {
    const { r, c } = cellpos;
    const result = pos.copy();

    const cellOffset = new Vector2([c * cellSize, (rows - r - 1) * cellSize]);
    result.add(cellOffset);

    return result;
  }
  private prepareToDraw() {
    const {
      ctx,
      canvas: { width, height }
    } = this.state;
    ctx.font = "10px Arial";
    ctx.fillStyle = "#FFF";
    ctx.strokeStyle = "#000";
    ctx.lineWidth = 1;
    ctx.fillRect(0, 0, width, height);
    ctx.strokeRect(0, 0, width, height);
  }

  // NEW PIECE
  private addNewPiece(nextPiece: Piece): boolean {
    const {
      props: { cols, rows },
      state: { cells }
    } = this;

    const pieceRows = nextPiece.cells.length;
    var pos = new ArrayPos(rows - 1 - pieceRows, Math.floor(cols / 2));

    for (var i = 0; i < pieceRows; i++) {
      if (nextPiece.isOverlap(cells, pos)) pos.add(Game.downPos);
      else break;
    }
    const finalOverlap = nextPiece.isOverlap(cells, pos);
    if (finalOverlap) {
      console.error(`having issue placing piece, pos = ${pos}`);
      return false;
    } else {
      this.state.currentPiece = new GamePiece(nextPiece.cells, pos);
      return true;
    }
  }
  private getNextPiece(): Piece {
    const {
      props: { colors }
    } = this;
    var nextPiece = getPiece();
    nextPiece.randomizeColors(colors.length);
    nextPiece.rotatePiece(Math.floor(Math.random() * 4));
    return nextPiece;
  }

  // MOVE AND ROTATE CURRENT PIECE
  private moveCurrentPiece(offset: ArrayPos): boolean {
    const {
      state: { cells, currentPiece }
    } = this;
    if (currentPiece) {
      var nextPos = currentPiece.pos.copy();
      nextPos.add(offset);
      var overlaps = currentPiece.isOverlap(cells, nextPos);
      if (!overlaps) {
        currentPiece.pos = nextPos;
        return true;
      }
    }
    return false;
  }
  private rotateCurrentPiece(): boolean {
    const {
      state: { cells, currentPiece }
    } = this;
    if (currentPiece) {
      currentPiece.rotatePiece();
      var overlaps = currentPiece.isOverlap(cells, currentPiece.pos);
      if (overlaps) {
        currentPiece.rotatePiece(3);
        return false;
      } else {
        this.state.currentPiece = new GamePiece(
          currentPiece.cells,
          currentPiece.pos
        );
        return true;
      }
    }

    return false;
  }

  // OVERLAP AND COUNTING
  private updateScore(removedLines: number) {
    const {
      state: { cells, level }
    } = this;
    var filledCount = 0;
    forEachCell(cells, (cellPos, cell) => (filledCount += cell > 0 ? 1 : 0));
    var boardCleared = filledCount === 0 && removedLines > 0;
    var points = 0;
    if (!boardCleared) {
      switch (removedLines) {
        case 1:
          points = 50 * (level + 1);
          break;
        case 2:
          points = 150 * (level + 1);
          break;
        case 3:
          points = 350 * (level + 1);
          break;
        case 4:
          points = 1000 * (level + 1);
          break;
      }
    } else {
      points = 2000 * (level + 1);
    }
    this.state.score += points;
  }
  private markCompletedLines() {
    const {
      props: { rows },
      state: { cells }
    } = this;

    var removedLines = 0;
    var r = 0;
    do {
      var row = cells[r];
      var isComplete = row.every(cell => cell > 0);
      if (isComplete) {
        cells[r] = row.map(c => -1);
        r++;
        removedLines++;
      } else {
        r++;
      }
    } while (r < rows);
    return removedLines;
  }
  private removeMarkedLines() {
    const {
      props: { rows, cols },
      state: { cells }
    } = this;

    var removedLines = 0;
    var r = 0;
    do {
      var row = cells[r];
      var isMarked = row.every(cell => cell === -1);
      if (isMarked) {
        cells.splice(r, 1);
        cells.push(createBoard(1, cols)[0]);
        removedLines++;
      } else {
        r++;
      }
    } while (r < rows);
    return removedLines;
  }
}
