import { Piece, pieces, rotate } from "../src/game/Piece";

test("renders learn react link", () => {
  var piece = pieces[0];
  var rotatedPiece = rotate(piece);
  expect(rotatedPiece.cells.length).toEqual(piece.cells[0].length);
  expect(rotatedPiece.cells[0].length).toEqual(piece.cells.length);
});
